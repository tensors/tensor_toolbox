function tf = isincomplete(X)
tf = strcmp(X.type,'incomplete');
return