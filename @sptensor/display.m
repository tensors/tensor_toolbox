function display(t)
%DISPLAY Command window display of a sptensor.
%
%   DISPLAY(T) displays the tensor with its name.
%
%   See also SPTENSOR, SPTENSOR/DISP.
%
%Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>



disp(t,inputname(1));
